package com.toyin.kreepykrypto;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CryptoActivity extends AppCompatActivity {
    private static final String QUERY_FOR_CRYPTO2 ="https://api.coingecko.com/api/v3/coins/markets?vs_currency=eur&order=market_cap_desc&per_page=100&page=1&sparkline=false";
    private CryptoConverter converter;
    private static final int TOPCRYPTO = 1;
    private ActionBar actionBar;
    private CryptoAdapter adapter;
    private List cSortingList;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kreepykryto);
        init();
        actionBarSetup();
        /**MockUp Data*/

//        cryptoList =new ArrayList<>();
//        Crypto crypto= new Crypto();
//        crypto.setName("bitcoin");
//        crypto.setPrice("43,0000");
//        crypto.setTotalVolume("7777777");
//        crypto.setTotalSupply("88888888");
//        crypto.setSymbol("bitcoinSym");
//        crypto.setLastUpdated("today");
//        crypto.setPriceChange24h("888");
//        crypto.setMarketCap("9999");
//        crypto.setLow24h("76");
//        crypto.setHigh24h("00000");
//        cryptoList.add(crypto);
//        cryptoList.add(crypto);
//        cryptoList.add(crypto);
    }
    private void init() {

        ImageView imageView = findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.ic_baseline_api_24);
        cSortingList=new ArrayList();
        converter=new CryptoConverter();
        cSortingList=converter.getCryptos();
        makeAPIRequest();

    }
    private void makeAPIRequest() {

        String url =QUERY_FOR_CRYPTO2;
        SingletonCrypto.getInstance(this.getApplicationContext()).getRequestQueue();


        JsonArrayRequest jsonArrayRequest= new JsonArrayRequest(Request.Method.GET,url,null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                converter.sendJsonToConverter(response);
                setRecyclerView();
                setTopCryptoView();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                converter.sendJsonToConverter(error);

            }
        });
        SingletonCrypto.getInstance(this).addToRequestQueue(jsonArrayRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.crpto_menu, menu);
        return true; // true zurückgeben damit das Menü angezeigt wird, anderenfalls wird es nicht angezeigt
    }

    private void SearchedToAdaptor(MenuItem searchedCrypto) {
        SearchView searchView = (SearchView) searchedCrypto.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
           /*query Collector*/
            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.cryptoSearch:
                 SearchedToAdaptor(item);
                 return true;
                case R.id.cryptoSortname:
              Collections.sort(cSortingList,Crypto.sortName);
               recyclerView.setAdapter(adapter);
                 return true;
            case R.id.cryptoSortPrice:
              Collections.sort(cSortingList,Crypto.sortPrice);
                recyclerView.setAdapter(adapter);
                 return true;
            case R.id.cryptoMarketCapSort:
                Collections.sort(cSortingList,Crypto.sortMarketCap);
                recyclerView.setAdapter(adapter);
                 return true;
           case R.id.contactUs:
               getContactForm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void getContactForm() {
        Intent intent =new Intent(this, ContactFormular.class);
        startActivity(intent);
    }


    private void actionBarSetup() {
        actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        // Icon setzen
        actionBar.setIcon(R.drawable.ic_baseline_api_24);
        // Prüfen ob vorhanden
        if (actionBar != null) {
            // Titel
            actionBar.setTitle("YorApp");
        }
    }

    private void setTopCryptoView() {
        TextView topCrypto=findViewById(R.id.TopCryptoView);
        TextView topCrypto2=findViewById(R.id.TopCryptoName);
        for (Crypto cryp:converter.getCryptos()) {
            if (Integer.parseInt(cryp.getRank())==TOPCRYPTO){
                topCrypto.setText(cryp.getPrice());
                topCrypto2.setText(cryp.getName());
            }

        }

    }

    private void setRecyclerView() {
     recyclerView = findViewById(R.id.CryptoRecyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        /*
            RecyclerView.Adapter
         */

         adapter = new CryptoAdapter(cSortingList);

        recyclerView.setAdapter(adapter);
        adapter.setOnCryptoClickListener(new CryptoAdapter.OnCryptoClickListener() {

            @Override
            public void onCryptoClick(Crypto crypto) {
              callDetailSite(crypto);
            }

        });

    }

    private void callDetailSite(Crypto crypto) {

        Intent intent =new Intent(this, CryptoDetailsActivity.class);
        intent.putExtra("SelectedCrypto",crypto);
        startActivity(intent);
    }


}
package com.toyin.kreepykrypto;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public final class SingletonCrypto {
        private static volatile SingletonCrypto INSTANCE=null;
        private RequestQueue requestQueue;
        private static Context ctx;

    private SingletonCrypto(Context context) {
        ctx=context;
            requestQueue = getRequestQueue();
    }

    /** Double checked locking is safer for multiple Threads (when more than one thread call for Resource
     * at the same time,they will be queued_up allowing only one Thread in,
     * copies of the created INSTANCE will  then be shared with the next thread on the queue) to ensure Thread safety*/
        public static SingletonCrypto getInstance(Context context) {
            if (INSTANCE == null) {
                synchronized (SingletonCrypto.class){
                    if (INSTANCE == null){
                        INSTANCE = new SingletonCrypto(context);
                    }
                }
            }
            return INSTANCE;
        }

        public RequestQueue getRequestQueue() {
            if (requestQueue == null) {
         /** getApplicationContext() is key, it keeps you from leaking the
           Activity or BroadcastReceiver if someone passes one in.*/
                requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
            }
            return requestQueue;
        }
    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}

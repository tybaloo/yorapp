package com.toyin.kreepykrypto;

import android.util.Log;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CryptoConverter {
    private  Crypto crypto;
    private List<Crypto> cryptos;

    public CryptoConverter() {
        this.cryptos =new ArrayList<>();
    }

    public void sendJsonToConverter(VolleyError error){
        error.getMessage();
    }

    public void sendJsonToConverter(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                try {
                    setCryptos(jsonToCrypto(jsonObject));
                } catch (IOException e) {e.printStackTrace();}
            } catch (JSONException e) {Log.e("JSON", e.getMessage());}
        }
    }

    private Crypto jsonToCrypto(JSONObject jsonObject) {
        crypto=new Crypto();
        try {
            crypto.setId(jsonObject.getString("id"));
            crypto.setName(jsonObject.getString("name"));
            crypto.setCryptoImage(jsonObject.getString("image"));
            crypto.setHigh24h(jsonObject.getString("high_24h"));
            crypto.setLow24h(jsonObject.getString("low_24h"));
            crypto.setMarketCap(jsonObject.getString("market_cap"));
            crypto.setPrice(jsonObject.getString("current_price"));
            crypto.setPriceChange24h(jsonObject.getString("price_change_24h"));
            crypto.setLastUpdated(jsonObject.getString("last_updated"));
            crypto.setSymbol(jsonObject.getString("symbol"));
            crypto.setTotalSupply(jsonObject.getString("total_supply"));
            crypto.setTotalVolume(jsonObject.getString("total_volume"));
            crypto.setRank(jsonObject.getString("market_cap_rank"));
            crypto.setCryptoImage(jsonObject.getString("market_cap_rank"));
            crypto.setCryptoImage(jsonObject.getString("image"));

        } catch (JSONException e) {Log.e("JSON", e.getMessage());}
        return crypto;
    }

    private  void setCryptos(Crypto crypto) throws IOException {
        cryptos.add(crypto);

    }

    public List<Crypto> getCryptos() {
        return cryptos;
    }
}

package com.toyin.kreepykrypto;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class CryptoDetailsActivity extends AppCompatActivity {
    ImageView cryptoImg;
    TextView price;
    TextView changes ;
    ImageView cryptoChartImg;
    TextView totalVol;
    TextView high_24h;
    TextView low_24h;
    Button backToHomeButton;
    Crypto crypto;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crypto_details);
        initializeView();
    }

    private void initializeView() {
        intent= getIntent();
        cryptoImg= findViewById(R.id.fragCryptoLog2);
        price = findViewById(R.id.fragCryptoPrice2);
        changes = findViewById(R.id.cryptoChanges2);
        cryptoChartImg = findViewById(R.id.cryptoCurrentChart2);
        totalVol= findViewById(R.id.crptoTotalVol2);
        high_24h= findViewById(R.id.cryptoHigh);
        low_24h= findViewById(R.id.cryptoLow);
        backToHomeButton = findViewById(R.id.fragbutton);

        setDetailsView(intent);
    }

    private void setDetailsView(Intent intent) {

        if(intent != null){
            if(intent.hasExtra("SelectedCrypto")){
                 crypto= (Crypto) intent.getSerializableExtra("SelectedCrypto");
                price.setText("€ "+crypto.getPrice()+" Euro");
                Glide.with(this).load(crypto.getCryptoImage()).into( cryptoImg);
                changes.setText("Änderungen in 24std : " +crypto.getPriceChange24h());
                totalVol.setText("Volle Lautstärke : " +crypto.getTotalVolume());
                high_24h.setText("Hoch : " +crypto.getHigh24h());
                low_24h.setText("Niedrig: " +crypto.getLow24h());
               //                cryptoChartImg


            }
        }
    }

    public void returnHome(View view) {
        finish();
    }

    public void showLastUpdate(View view) {
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        AlertDialog alertDialog = builder.create();
        alertDialog.setMessage("Letztes Update war: "+crypto.getLastUpdated());
        alertDialog.show();
    }

    public void showShareLink(View view) {

intent =new Intent(Intent.ACTION_SEND);
intent.setType("text/plain");
String shareBody = "Hallo ich benutze YORAPP um mich über Cryptowährungen zu  Informieren. clicken Sie auf die link: https://yorapp.toyin/";
String shareSubject= "YORAPP HERUNTERLADEN";
intent.putExtra(Intent.EXTRA_SUBJECT,shareSubject);
intent.putExtra(Intent.EXTRA_TEXT,shareBody);
startActivity(Intent.createChooser(intent,"Teilen Mit"));
    }

    public void walletClicked(View view) {
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        builder.setTitle("Crypto Wallet ");
        builder.setPositiveButton("ja",(dialog, which) -> callMeteMaskIntent(dialog, which));
        builder.setNegativeButton("Nein",(dialog, which) -> doNothing());
        AlertDialog alertDialog = builder.create();
        alertDialog.setMessage("Willst du wirklich eine Crypto Wallet erzeugen ?");
        alertDialog.show();


    }

    private void doNothing() {
        Log.i("Alert","nein , bitte nicht ");
    }

    private void callMeteMaskIntent(DialogInterface dialog, int which) {
       intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://metamask.io/"));
        startActivity(intent);

    }
}
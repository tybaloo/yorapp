package com.toyin.kreepykrypto;

import java.io.Serializable;
import java.util.Comparator;

public class Crypto  implements Serializable {
    private String id;
    private String name;
    private String Symbol;
    private String cryptoImage;
    private String totalVolume;
    private String Price;
    private String PriceChange24h;
    private String High24h;
    private String Low24h;
    private String marketCap;
    private String totalSupply;
    private String lastUpdated;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    private String rank;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String symbol) {
        Symbol = symbol;
    }

    public String getCryptoImage() {
        return cryptoImage;
    }

    public void setCryptoImage(String cryptoImage) {
        this.cryptoImage = cryptoImage;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getPriceChange24h() {
        return PriceChange24h;
    }

    public void setPriceChange24h(String priceChange24h) {
        PriceChange24h = priceChange24h;
    }

    public String getHigh24h() {
        return High24h;
    }

    public void setHigh24h(String high24h) {
        High24h = high24h;
    }

    public String getLow24h() {
        return Low24h;
    }

    public void setLow24h(String low24h) {
        Low24h = low24h;
    }

    public String getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(String marketCap) {
        this.marketCap = marketCap;
    }

    public String getTotalSupply() {
        return totalSupply;
    }

    public void setTotalSupply(String totalSupply) {
        this.totalSupply = totalSupply;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }




    public static Comparator<Crypto> sortName = new Comparator<Crypto>() {
            @Override
            public int compare(Crypto c1, Crypto c2) {
                return c1.getName().compareToIgnoreCase(c2.getName()) ;
            }
        };

    public static Comparator<Crypto> sortMarketCap = new Comparator<Crypto>() {
        @Override
        public int compare(Crypto c1, Crypto c2) {
            return Double.valueOf(c2.marketCap).compareTo(Double.valueOf(c1.marketCap));
        }
    };

    public static Comparator<Crypto> sortTotalVol = new Comparator<Crypto>() {
        @Override
        public int compare(Crypto c1, Crypto c2) {
            return Double.valueOf(c2.totalVolume).compareTo(Double.valueOf(c1.totalVolume));
        }
    };
    public static Comparator<Crypto> sortPrice =  new Comparator<Crypto>(){
        public int compare(Crypto obj1, Crypto obj2) {
           return Double.valueOf(obj2.Price).compareTo(Double.valueOf(obj1.Price)); // To compare integer values
        }
    };

//
}

package com.toyin.kreepykrypto;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

public class CryptoFragment extends Fragment {
    private Crypto crypto;
    private View cryptoRootView;
    Button backButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        cryptoRootView = inflater.inflate(R.layout.fragment_crypto_display, container, false);
        return cryptoRootView;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (crypto != null) {
            TextView viewCryptoPrice = cryptoRootView.findViewById(R.id.fragCryptoPrice2);
            TextView viewCryptoChange = cryptoRootView.findViewById(R.id.cryptoChanges2);
            TextView viewCryptoTotalVol = cryptoRootView.findViewById(R.id.crptoTotalVol2);
//            TextView viewCryptoHigh = cryptoRootView.findViewById(R.id.fragCryptoHigh);
//            TextView viewCryptoLow = cryptoRootView.findViewById(R.id.fragCryptoLow);
           ImageView viewCryptoChart = cryptoRootView.findViewById(R.id.fragCryptoLog2);
            viewCryptoPrice.setText("€ "+crypto.getPrice()+" Euro");
            viewCryptoChange.setText("Änderungen in 24std : " +crypto.getPriceChange24h());
            viewCryptoTotalVol.setText("Volle Lautstärke : " +crypto.getTotalVolume());
//            viewCryptoHigh.setText("Hoch : " +crypto.getHigh24h());
//            viewCryptoLow.setText("Niedrig: " +crypto.getLow24h());
            Glide.with(cryptoRootView).load(crypto.getCryptoImage()).into(viewCryptoChart);
            backButton= getActivity().findViewById(R.id.fragbutton);
            backButton.setOnClickListener(v->returnHome());


        }

    }

    private void returnHome() {
        getActivity().getSupportFragmentManager().popBackStackImmediate();
    }



    public static CryptoFragment newInstance(Crypto crypto) {
        CryptoFragment cryptoFragment = new CryptoFragment();
        cryptoFragment.setCrypto(crypto);

        return cryptoFragment;
    }


    public void setCrypto(Crypto crypto) {
        this.crypto = crypto;
    }
}
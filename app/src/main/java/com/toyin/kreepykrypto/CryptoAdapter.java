package com.toyin.kreepykrypto;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CryptoAdapter extends RecyclerView.Adapter<CryptoAdapter.CryptoViewHolder> implements Filterable {
    private List<Crypto> cryptos;
    private List<Crypto> cryptosFiltered;
    private  View view;
    private OnCryptoClickListener cryptoClickListener;  // (2) Instanzvariable vom Typ des Interfaces

    @Override
    public Filter getFilter() {
        return cryptoFilterer;
    }

    private Filter cryptoFilterer = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Crypto>filteredCryptos= new ArrayList<>();
            if(constraint== null || constraint.length()==0){
                filteredCryptos.addAll(cryptosFiltered);
            }
            else{
                String filterParttern = constraint.toString().toLowerCase().trim();
                for (Crypto cryp: cryptosFiltered) {
                    if(cryp.getName().toLowerCase().startsWith(filterParttern)){
                       filteredCryptos.add(cryp);
                    }
                }
            }
            /**
             * Holds the results of a filtering operation. The results are the values
             * computed by the filtering operation and the number of these values.
             */
    FilterResults results = new FilterResults();
            results.values= filteredCryptos;
            return  results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            cryptos.clear();
            cryptos.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public interface OnCryptoClickListener {
        void onCryptoClick(Crypto crypto);
    }



    // (3) einen Setter für die Instanzvariable
    public void setOnCryptoClickListener(OnCryptoClickListener cryptoClickListener) {
        this.cryptoClickListener = cryptoClickListener;
    }

    public CryptoAdapter(List<Crypto> cryptos) {
        this.cryptos = cryptos;
        cryptosFiltered = new ArrayList<>(cryptos);
    }

    public class CryptoViewHolder extends RecyclerView.ViewHolder {
        TextView cryptoName;
        TextView cryptoPrice;
        TextView cryptoAbbr;
        TextView cryptoUprate;
        ImageView cryptoImage;

        // (4) Konstruktor: Parameter vom Typ des Interfaces hinzufügen
        public CryptoViewHolder(View itemView, OnCryptoClickListener cryptoClickListener) {
            super(itemView);
            cryptoName = itemView.findViewById(R.id.cryptoName);
            cryptoImage=itemView.findViewById(R.id.cryptoLogo);
            cryptoPrice = itemView.findViewById(R.id.cryptoPrice);
            cryptoAbbr = itemView.findViewById(R.id.cryptoAbb);
            cryptoUprate= itemView.findViewById(R.id.cryptoUprate);
            view=itemView;

            // als Lambda
            itemView.setOnClickListener(view -> {
                int position = getAdapterPosition();
                cryptoClickListener.onCryptoClick(cryptos.get(position));
            });
        }
    }

    @Override
    public CryptoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.crypt_recyclerview_item, parent, false);
        return new  CryptoViewHolder(view, cryptoClickListener); // (5) Eigenen Listener übergeben
    }

    @Override
    public void onBindViewHolder( CryptoViewHolder holder, int position) {
        Crypto crypto = cryptos.get(position);
        holder.cryptoName.setText(crypto.getName());
        holder.cryptoAbbr.setText(crypto.getSymbol());
        holder.cryptoUprate.setText(crypto.getHigh24h());
        holder.cryptoPrice.setText(crypto.getPrice());
        Glide.with(view).load(crypto.getCryptoImage()).into(holder.cryptoImage);
    }

    @Override
    public int getItemCount() {
        return cryptos.size();
    }
}

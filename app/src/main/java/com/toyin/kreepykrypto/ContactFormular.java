package com.toyin.kreepykrypto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ContactFormular extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_formular);
        Intent intent= getIntent();


        EditText subject= findViewById(R.id.editText_email_subject);
        EditText message= findViewById(R.id.editText_email_message);
        Button btn= findViewById(R.id.submitContactForm);
        Button btn2= findViewById(R.id.buttonBackFromContact);

        if(intent != null){

            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String msg=message.getText().toString().trim();
                    String sub=subject.getText().toString().trim();
                    String email = "yorapp2@gmail.com";

                    if(!(sub.isEmpty()&& msg.isEmpty())){
                        String mail=
                                "mailto:"+email+
                                        "?&subject="+ Uri.encode(sub)+
                                        "&body="+Uri.encode(msg);
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse(mail));
                        try{
                            startActivity(intent.createChooser(intent,"Send Mail..."));}
                        catch (Exception exception) {
                            exception.getMessage();
                        }


                    }

                }
            });
        }
        else{
            finish();
        }



    }
}